package com.umss.data.dataservice.controller;

/**
 * @autor Juan Montaño
 */
public final class Constants {

    public final class BasePath{
        public static final String EMPLOYEES = "/employees";
        public static final String DEPARTAMENTS = "/departaments";
        public static final String VACATIONS = "/vacations";
    }
}
