package com.umss.data.dataservice.model.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @autor Juan Montaño
 */
@Entity
public class Direction implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "directionid")
    private Long id;

    private String calle;

    private Integer NHouse;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public Integer getNHouse() {
        return NHouse;
    }

    public void setNHouse(Integer NHouse) {
        this.NHouse = NHouse;
    }
}
