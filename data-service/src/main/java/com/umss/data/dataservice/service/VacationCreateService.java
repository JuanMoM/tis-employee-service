package com.umss.data.dataservice.service;

import com.umss.data.dataservice.client.service.SystemVacationService;
import com.umss.data.dataservice.client.vacation.input.VacationInput;
import com.umss.data.dataservice.client.vacation.model.Vacation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @autor Juan Montaño
 */
@Service
public class VacationCreateService {

    private VacationInput input;

    private Vacation vacation;

    @Autowired
    private SystemVacationService service;

    public void execute() {
        vacation = service.createVacation(input);
    }

    public void setInput(VacationInput input) {
        this.input = input;
    }

    public Vacation getVacation() {
        return vacation;
    }
}
