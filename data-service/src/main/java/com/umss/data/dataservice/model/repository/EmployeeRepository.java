package com.umss.data.dataservice.model.repository;

import com.umss.data.dataservice.model.domain.EmployeeImpl;
import org.hibernate.sql.Select;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * @autor Juan Montaño
 */
public interface EmployeeRepository extends CrudRepository<EmployeeImpl, Long> {

    Optional<EmployeeImpl> findByIdAndFirstName(Long id, String firstName);

    @Query("select item from EmployeeImpl item where item.id = :id or item.firstName = :firstName")
    Optional<EmployeeImpl> findByIdOrFirstName(@Param("id") Long id, @Param("firstName") String firstName);
}
