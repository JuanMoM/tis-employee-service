package com.umss.data.dataservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @autor Juan Montaño
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class EmployeeNotFoundExeption extends RuntimeException{

    public EmployeeNotFoundExeption(String mesnsage) {
        super(mesnsage);
    }
}
