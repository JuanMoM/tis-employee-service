package com.umss.data.dataservice.client.service;

import com.umss.data.dataservice.client.Constants;
import com.umss.data.dataservice.client.vacation.input.VacationInput;
import com.umss.data.dataservice.client.vacation.model.Vacation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @autor Juan Montaño
 */
@FeignClient(name = Constants.VACATION_SERVICE_NAME)
public interface SystemVacationClient {

    @RequestMapping(
            method = RequestMethod.POST,
            value = "vacations")
    Vacation createVacation(@RequestBody VacationInput input);
}
