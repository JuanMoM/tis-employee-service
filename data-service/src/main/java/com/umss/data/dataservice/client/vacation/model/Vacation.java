package com.umss.data.dataservice.client.vacation.model;

import java.util.Date;

/**
 * @autor Juan Montaño
 */
public class Vacation implements com.umss.vacation.api.model.Vacation {

    private Long id;

    private Date dataInit;

    private Date dataFin;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Date getDataInit() {
        return dataInit;
    }

    @Override
    public Date getDataFin() {
        return dataFin;
    }
}
