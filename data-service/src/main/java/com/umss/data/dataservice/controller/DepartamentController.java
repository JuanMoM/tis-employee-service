package com.umss.data.dataservice.controller;

import com.umss.data.api.input.DepartamentInput;
import com.umss.data.api.model.Departament;
import com.umss.data.dataservice.service.DepartamentCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * @autor Juan Montaño
 */

@Api(
        tags = "Rest-Departament-Controller",
        description = "Operation over Departament"
)
@RestController
@RequestMapping(value = Constants.BasePath.DEPARTAMENTS)
public class DepartamentController {

    @Autowired
    private DepartamentCreateService departamentCreateService;

    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "create a Departament")
    @RequestMapping(method = RequestMethod.POST)
    public Departament createDepartament(@RequestBody DepartamentInput input) {
        departamentCreateService.setInput(input);
        departamentCreateService.execute();

        return departamentCreateService.getDepartament();
    }
}
