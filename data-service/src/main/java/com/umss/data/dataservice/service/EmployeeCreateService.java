package com.umss.data.dataservice.service;

import com.umss.data.api.input.EmployeeInput;
import com.umss.data.dataservice.exception.DepartamentNotFoundException;
import com.umss.data.dataservice.model.domain.Departament;
import com.umss.data.dataservice.model.domain.Direction;
import com.umss.data.dataservice.model.domain.EmployeeImpl;
import com.umss.data.dataservice.model.repository.DepartamentRepository;
import com.umss.data.dataservice.model.repository.DiredctionRepository;
import com.umss.data.dataservice.model.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @autor Juan Montaño
 */
@Service
public class EmployeeCreateService {

    private EmployeeInput input;

    private EmployeeImpl employeeImplement;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private DepartamentRepository departamentRepository;

    @Autowired
    private DiredctionRepository diredctionRepository;

    public void execute() {

        Departament departamentInstance = findDepartament(input.getIdDepartament());

        employeeImplement = employeeRepository.save(composeEmployee(departamentInstance));
    }

    private Departament findDepartament(Long idDepartament) {
        return departamentRepository.findById(idDepartament)
                .orElseThrow(()-> new DepartamentNotFoundException("not found departament for id: "+idDepartament));
    }

    private EmployeeImpl composeEmployee(Departament instanceDepartament) {

        Direction direction = new Direction();
        direction.setCalle(input.getCalle());
        direction.setNHouse(input.getNHouse());

        direction = diredctionRepository.save(direction);

        EmployeeImpl employeeImplement = new EmployeeImpl();
        employeeImplement.setFirstName(input.getFirstName());
        employeeImplement.setLastName(input.getLastName());
        employeeImplement.setIdAdministrative(input.getIdAdministrative());
        employeeImplement.setDirection(direction);
        employeeImplement.setDepartament(instanceDepartament);

        return employeeImplement;
    }

    public void setInput(EmployeeInput input) {
        this.input = input;
    }

    public EmployeeImpl getEmployeeImplement() {
        return employeeImplement;
    }
}
