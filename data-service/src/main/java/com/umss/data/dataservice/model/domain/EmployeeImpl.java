package com.umss.data.dataservice.model.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.umss.data.api.model.Employee;

import javax.persistence.*;

/**
 * @autor Juan Montaño
 */
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EmployeeImpl implements Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(unique = true)
    private String firstName;

    private String lastName;

    private Long idAdministrative;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "directionid", referencedColumnName = "directionid")
    private Direction direction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "departamentid", referencedColumnName = "departamentid")
    private Departament departament;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public Long getIdAdministrative() {
        return idAdministrative;
    }

    public void setIdAdministrative(Long idAdministrative) {
        this.idAdministrative = idAdministrative;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public com.umss.data.api.model.Departament getDepartament() {
        return departament;
    }

    public void setDepartament(Departament departament) {
        this.departament = departament;
    }
}
