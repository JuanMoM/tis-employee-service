package com.umss.data.dataservice.model.repository;

import com.umss.data.dataservice.model.domain.Departament;
import org.springframework.data.repository.CrudRepository;

/**
 * @autor Juan Montaño
 */
public interface DepartamentRepository extends CrudRepository<Departament, Long> {
}
