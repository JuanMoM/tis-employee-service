package com.umss.data.dataservice.controller;

import com.umss.data.dataservice.client.vacation.input.VacationInput;
import com.umss.data.dataservice.client.vacation.model.Vacation;
import com.umss.data.dataservice.service.VacationCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * @autor Juan Montaño
 */

@Api(
        tags = "Rest-Vacation-Controller",
        description = "Operation over Vacation")
@RestController
@RequestMapping(value = Constants.BasePath.VACATIONS)
public class VacationController {

    @Autowired
    private VacationCreateService service;

    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "create a vacation")
    @RequestMapping(method = RequestMethod.POST)
    public Vacation createVacation(@RequestBody VacationInput input) {
        service.setInput(input);
        service.execute();

        return service.getVacation();
    }
}
