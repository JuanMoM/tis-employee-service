package com.umss.data.dataservice.client;

/**
 * @autor Juan Montaño
 */
public final class Constants {

    public static final String VACATION_SERVICE_NAME = "vacation-service";

    public Constants() {
    }
}
