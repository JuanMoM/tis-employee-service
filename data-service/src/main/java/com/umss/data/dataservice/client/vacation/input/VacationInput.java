package com.umss.data.dataservice.client.vacation.input;

import java.util.Date;

/**
 * @autor Juan Montaño
 */
public class VacationInput {

    private Date dataInit;

    private Date dataFin;

    public Date getDataInit() {
        return dataInit;
    }

    public void setDataInit(Date dataInit) {
        this.dataInit = dataInit;
    }

    public Date getDataFin() {
        return dataFin;
    }

    public void setDataFin(Date dataFin) {
        this.dataFin = dataFin;
    }
}
