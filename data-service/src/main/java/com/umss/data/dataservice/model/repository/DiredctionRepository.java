package com.umss.data.dataservice.model.repository;

import com.umss.data.dataservice.model.domain.Direction;
import org.springframework.data.repository.CrudRepository;

/**
 * @autor Juan Montaño
 */
public interface DiredctionRepository extends CrudRepository<Direction, Long> {
}
