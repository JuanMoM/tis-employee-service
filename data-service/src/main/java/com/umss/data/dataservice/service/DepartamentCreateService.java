package com.umss.data.dataservice.service;

import com.umss.data.api.input.DepartamentInput;
import com.umss.data.dataservice.model.domain.Departament;
import com.umss.data.dataservice.model.repository.DepartamentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @autor Juan Montaño
 */

@Service
public class DepartamentCreateService {

    private DepartamentInput input;

    private Departament departament;

    @Autowired
    private DepartamentRepository departamentRepository;

    public void execute() {

        Departament instance = composeDepartament();

        departament = departamentRepository.save(instance);
    }

    private Departament composeDepartament() {

        Departament departament = new Departament();
        departament.setName(input.getName());

        return departament;
    }

    public void setInput(DepartamentInput input) {
        this.input = input;
    }

    public Departament getDepartament() {
        return departament;
    }
}
