package com.umss.data.dataservice.client.service;

import com.umss.data.dataservice.client.vacation.input.VacationInput;
import com.umss.data.dataservice.client.vacation.model.Vacation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @autor Juan Montaño
 */

@Service
public class SystemVacationService {

    @Autowired
    private SystemVacationClient systemVacationClient;

    public Vacation createVacation(VacationInput input) {

        return systemVacationClient.createVacation(input);
    }
}
