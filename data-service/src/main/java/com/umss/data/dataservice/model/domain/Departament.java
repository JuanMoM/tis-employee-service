package com.umss.data.dataservice.model.domain;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;

/**
 * @autor Juan Montaño
 */
@Entity
public class Departament implements com.umss.data.api.model.Departament {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "departamentid")
    Long id;

    String name;


    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Autowired
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
