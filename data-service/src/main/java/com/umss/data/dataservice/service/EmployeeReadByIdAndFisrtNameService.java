package com.umss.data.dataservice.service;

import com.umss.data.dataservice.exception.EmployeeNotFoundExeption;
import com.umss.data.dataservice.model.domain.EmployeeImpl;
import com.umss.data.dataservice.model.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @autor Juan Montaño
 */
@Service
public class EmployeeReadByIdAndFisrtNameService {

    private Long id;

    private String firstName;

    private EmployeeImpl employee;

    @Autowired
    private EmployeeRepository repository;

    public void execute() {
        employee = findEmployee();
    }

    private EmployeeImpl findEmployee() {

        return repository.findByIdAndFirstName(id, firstName)
                .orElseThrow(() -> new EmployeeNotFoundExeption("not exist employee for id: " + id + " and firstName: " + firstName));
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public EmployeeImpl getEmployee() {
        return employee;
    }
}
