package com.umss.data.dataservice.controller;

import com.umss.data.api.input.EmployeeInput;
import com.umss.data.api.model.Employee;
import com.umss.data.dataservice.model.domain.EmployeeImpl;
import com.umss.data.dataservice.service.EmployeeCreateService;
import com.umss.data.dataservice.service.EmployeeReadByIdAndFisrtNameService;
import com.umss.data.dataservice.service.EmployeeReadByIdOrFirstNameService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * @autor Juan Montaño
 */
@Api(
        tags = "Rest-employee-controller",
        description = "Operatión over Employee")
@RestController
@RequestMapping(value = Constants.BasePath.EMPLOYEES)
public class EmployeeController {

    @Autowired
    private EmployeeCreateService employeeCreateService;

    @Autowired
    private EmployeeReadByIdAndFisrtNameService employeeReadByIdAndFisrtNameService;

    @Autowired
    private EmployeeReadByIdOrFirstNameService employeeReadByIdOrFirstNameService;

    @ApiResponses(
            @ApiResponse(code = 201, message = "employee created")
    )
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "create an employee")
    @RequestMapping(method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody EmployeeInput input) {
        employeeCreateService.setInput(input);
        employeeCreateService.execute();

        return employeeCreateService.getEmployeeImplement();
    }

    @ApiOperation(value = "find an Employee by id and first name")
    @RequestMapping(value = "/readByIdAndFisrtName", method = RequestMethod.GET)
    public EmployeeImpl readEmployeeByIdAndFirstName(@RequestParam Long id, @RequestParam String fisrtName) {

        employeeReadByIdAndFisrtNameService.setId(id);
        employeeReadByIdAndFisrtNameService.setFirstName(fisrtName);
        employeeReadByIdAndFisrtNameService.execute();

        return employeeReadByIdAndFisrtNameService.getEmployee();
    }

    @ApiOperation(value = "find an Employee by id or first name")
    @RequestMapping(value = "/byIdOrFirstName", method = RequestMethod.GET)
    public EmployeeImpl readEmployeeByIdOrFirstName(@RequestParam Long id, @RequestParam String firstName) {
        employeeReadByIdOrFirstNameService.setId(id);
        employeeReadByIdOrFirstNameService.setFirstName(firstName);
        employeeReadByIdOrFirstNameService.execute();

        return employeeReadByIdOrFirstNameService.getEmployee();
    }
}
