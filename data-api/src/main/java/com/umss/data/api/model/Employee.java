package com.umss.data.api.model;

/**
 * @autor Juan Montaño
 */
public interface Employee {

    Long getId();

    String getFirstName();

    String getLastName();

    Long getIdAdministrative();

}
