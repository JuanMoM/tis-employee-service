package com.umss.data.api.input;

/**
 * @autor Juan Montaño
 */
public class EmployeeInput {

    private String firstName;

    private String lastName;

    private Long idAdministrative;

    private Long idDepartament;

    private String calle;

    private Integer NHouse;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getIdAdministrative() {
        return idAdministrative;
    }

    public void setIdAdministrative(Long idAdministrative) {
        this.idAdministrative = idAdministrative;
    }

    public Long getIdDepartament() {
        return idDepartament;
    }

    public void setIdDepartament(Long idDepartament) {
        this.idDepartament = idDepartament;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public Integer getNHouse() {
        return NHouse;
    }

    public void setNHouse(Integer NHouse) {
        this.NHouse = NHouse;
    }
}
