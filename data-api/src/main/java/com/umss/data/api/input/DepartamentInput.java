package com.umss.data.api.input;

/**
 * @autor Juan Montaño
 */
public class DepartamentInput {

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
