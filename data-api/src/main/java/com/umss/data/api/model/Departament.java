package com.umss.data.api.model;

/**
 * @autor Juan Montaño
 */

public interface Departament {

    public Long getId();

    public String getName();
}
